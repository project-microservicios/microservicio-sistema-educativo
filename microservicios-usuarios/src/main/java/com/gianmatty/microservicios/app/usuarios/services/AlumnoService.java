package com.gianmatty.microservicios.app.usuarios.services;

import com.gianmatty.microservicios.commons.services.CommonService;

import java.util.List;

import com.gianmatty.microservicios.commons.alumnos.models.entity.Alumno;


public interface AlumnoService extends CommonService<Alumno> {
	
	public List<Alumno> findByNombreOrApellido(String term);
}
