package com.gianmatty.microservicios.app.usuarios.models.repository;

import com.gianmatty.microservicios.commons.alumnos.models.entity.Alumno;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface AlumnoRepository extends CrudRepository<Alumno, Long> {
	
	//CONSULTAS USANDO QUERYS: esto no es de SQL sino de HQL de hibernate
	@Query("select a from Alumno a where a.nombre like %?1% or a.apellido like %?1%")
	public List<Alumno> findByNombreOrApellido(String term);
}
