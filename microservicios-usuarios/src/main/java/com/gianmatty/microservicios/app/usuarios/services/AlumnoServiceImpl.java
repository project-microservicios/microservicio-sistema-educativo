package com.gianmatty.microservicios.app.usuarios.services;

import com.gianmatty.microservicios.commons.alumnos.models.entity.Alumno;
import com.gianmatty.microservicios.app.usuarios.models.repository.AlumnoRepository;
import com.gianmatty.microservicios.commons.services.CommonServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service //para registrar este componente en el servidor de spring para luego inyectar en los controladores con autowired
public class AlumnoServiceImpl extends CommonServiceImpl<Alumno, AlumnoRepository> implements AlumnoService{

	@Override
	@Transactional(readOnly = true)
	public List<Alumno> findByNombreOrApellido(String term) {
		return repository.findByNombreOrApellido(term);
	}


}
