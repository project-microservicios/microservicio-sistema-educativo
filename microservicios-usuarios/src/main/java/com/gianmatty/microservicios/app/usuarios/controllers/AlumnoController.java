package com.gianmatty.microservicios.app.usuarios.controllers;

import com.gianmatty.microservicios.commons.alumnos.models.entity.Alumno;
import com.gianmatty.microservicios.app.usuarios.services.AlumnoService;
import com.gianmatty.microservicios.commons.controllers.CommonController;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class AlumnoController extends CommonController<Alumno, AlumnoService>{

    @PutMapping("/{id}")
    public ResponseEntity<?> editar(@RequestBody Alumno alumno, @PathVariable Long id){
        Optional<Alumno> o = service.findById(id);

        if(o.isPresent()){
            Alumno alumnoDb = o.get();
            alumnoDb.setNombre(alumno.getNombre());
            alumnoDb.setApellido(alumno.getApellido());
            alumnoDb.setEmail(alumno.getEmail());
            return ResponseEntity.status(HttpStatus.CREATED).body(service.save(alumnoDb)); //"CREATED" = 201
        }
        return ResponseEntity.notFound().build(); //"notFound" = 404
    }
    
    @GetMapping("/filtrar/{term}")
    public ResponseEntity<?> filtrar(@PathVariable String term){
    	return ResponseEntity.ok(service.findByNombreOrApellido(term));
    }

}
