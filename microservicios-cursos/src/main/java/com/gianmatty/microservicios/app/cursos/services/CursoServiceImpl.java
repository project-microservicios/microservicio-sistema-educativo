package com.gianmatty.microservicios.app.cursos.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gianmatty.microservicios.app.cursos.models.entity.Curso;
import com.gianmatty.microservicios.app.cursos.models.repository.CursoRepository;
import com.gianmatty.microservicios.commons.services.CommonServiceImpl;
import com.gianmatty.microservicios.app.cursos.services.CursoService;

@Service //para registrarlo como componente y se pueda inyectar
public class CursoServiceImpl extends CommonServiceImpl<Curso, CursoRepository> implements CursoService{

	@Override
	@Transactional(readOnly = true)
	public Curso findCursoByAlumnoId(Long id) {
		return repository.findCursoByAlumnoId(id);
	}

}
