package com.gianmatty.microservicios.app.cursos.services;

import com.gianmatty.microservicios.app.cursos.models.entity.Curso;
import com.gianmatty.microservicios.commons.services.CommonService;

public interface CursoService extends CommonService<Curso> {

	public Curso findCursoByAlumnoId(Long id);
}
