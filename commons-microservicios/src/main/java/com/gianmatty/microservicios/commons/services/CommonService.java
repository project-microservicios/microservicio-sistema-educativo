package com.gianmatty.microservicios.commons.services;

import java.util.Optional;

public interface CommonService<E> {

    //El Iterable y Optional son un  List con mejoras

    public Iterable<E> findAll();

    public Optional<E> findById(Long id);

    public E save(E entity);

    public void deleteById(Long id);


}
