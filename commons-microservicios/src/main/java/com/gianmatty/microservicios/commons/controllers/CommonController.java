package com.gianmatty.microservicios.commons.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.gianmatty.microservicios.commons.services.CommonService;

import java.util.Optional;

public class CommonController<E, S extends CommonService<E>> { //Generico "E" es el entity y "S" es el service

    @Autowired
    protected S service; //siempre se debe importar tipos genericos osea la interface no la implementacion ya que este puede cambiar

    @GetMapping
    public ResponseEntity<?> listar(){ // "?" para poner guardar cualquier tipo de datos
        return ResponseEntity.ok().body(service.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> ver(@PathVariable Long id){
        Optional<E> o = service.findById(id);
        if(o.isPresent()){
            return ResponseEntity.ok(o.get()); //"ok" = 200
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<?> crear(@RequestBody E entity){
        E entityDb = service.save(entity);
        return ResponseEntity.status(HttpStatus.CREATED).body(entityDb);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> eliminar(@PathVariable Long id){
        service.deleteById(id);
        return ResponseEntity.noContent().build(); //"noContent = 204"/
    }
}
