package com.gianmatty.microservicios.commons.alumnos.models.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "usuarios")
public class Alumno {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /*Como las tablas se van a llamar igual no es necesario @Column igualmente se va mapear*/
    private String nombre;
    private String apellido;
    private String email;
    @Column(name = "create_at")
    @Temporal(TemporalType.TIMESTAMP) //para que me de la fecha completa mas hora
    private Date createAt;

    @PrePersist /*para que la fecha nose ponga desde la BD sino desde aqui*/
    public void prePersist(){
        this.createAt = new Date();
    }

    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }


    //Entender este metodo que es para eliminar
	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(!(obj instanceof Alumno)) {
			return false;
		}
		Alumno a = (Alumno) obj;
		
		return this.id != null && this.id.equals(a.getId());
	}
    
    
}
